*** Settings ***
Library  Selenium2Library

*** Variables ***
${Browser}  Firefox
${URL}  https://kumparan.com
${Trending_News}  //a[@href='/trending']
${Video_News}  //a[@href='/topic/kumparan-video']
${Liputan_Khusus}  //a[@href='/topic/liputan-khusus']
${Honor_News}  //a[@href='/topic/honor']
*** Test Cases ***

TC_Show_News_In_Trending
    open browser  ${url}  ${browser}
    wait until page contains element  ${Trending_News}
    click element  ${Trending_News}
    sleep  5s
    close browser


TC_Show_News_In_Video
    open browser  ${url}  ${browser}
    click element  ${Video_news}
    sleep  5s
    close browser

TC_Show_News_In_Liputan
    open browser  ${url}  ${browser}
    click element  ${Liputan_Khusus}
    sleep  5s
    close browser

TC_Show_News_In_Honor
    open browser  ${url}  ${browser}
    wait until page contains element  ${Honor_News}
    click element  ${Honor_News}
    sleep  5s
    close browser

TC_Show_Detail_News
    open browser  ${url}  ${browser}
    sleep  3s
    click element  xpath=/html/body/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div[2]/div/div/div[1]/div/a/div[1]/div/span
    sleep  5s
    close browser
