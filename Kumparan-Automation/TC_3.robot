*** Settings ***
Library  Selenium2Library

*** Variables ***
${Browser}  Firefox
${URL}  https://kumparan.com
*** Test Cases ***

TC_Post_Comment_Without_Login
    open browser  ${url}  ${browser}
    sleep  2s
    click element  xpath=/html/body/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div[2]/div/div/div[1]/div/a/div[1]/div/span
    sleep  2s
    click element  xpath=/html/body/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div/div[2]/div/div/div/div[2]/div/div[1]/div/div[1]/div/div/div/div/div[2]/div/div/div/div
    input text  css=.notranslate  .test comment
    click element  //a[@data-qa-id='btn-send-comment']
    sleep  2s
    close browser

TC_Post_Comment_and_Login
    open browser  ${url}  ${browser}
    sleep  2s
    click element  xpath=/html/body/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div[2]/div/div/div[1]/div/a/div[1]/div/span
    sleep  2s
    click element  xpath=/html/body/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div/div[2]/div/div/div/div[2]/div/div[1]/div/div[1]/div/div/div/div/div[2]/div/div/div/div
    input text  css=.notranslate  .test comment
    click element  //a[@data-qa-id='btn-send-comment']
    sleep  2s
    click button  css=.Modalweb__ModalContent-ivd8dh-1 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(3) > div:nth-child(1) > button:nth-child(1)
    sleep  3s
    select window  NEW
    maximize browser window
    wait until page contains element  //input[@name='login']
    input text  id:email  claudiaartia@yahoo.co.id
    input text  id:pass  siclaudia
    click element  //input[@name='login']
    sleep  5s
    close browser

TC_Post_Blank_Comment
    open browser  ${url}  ${browser}
    sleep  2s
    click element  xpath=/html/body/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div[2]/div/div/div[1]/div/a/div[1]/div/span
    sleep  2s
    click element  //a[@data-qa-id='btn-send-comment']
    sleep  2s
    close browser
