*** Settings ***
Library  Selenium2Library

*** Variables ***
${Browser}  Firefox
${URL}  https://kumparan.com

*** Test Cases ***
TC_Valid_FB_Login
    open browser  ${URL}  ${Browser}
    wait until page contains element  //button[@data-qa-id='btn-login-fb']
    click element  //button[@data-qa-id='btn-login-fb']
    sleep  3s
    select window  NEW
    maximize browser window
    wait until page contains element  //input[@name='login']
    input text  id:email  claudiaartia@yahoo.co.id
    input text  id:pass  siclaudia
    click element  //input[@name='login']
    sleep  5s
    close browser

TC_Invalid_FB_Login_1
    open browser  ${URL}  ${Browser}
    wait until page contains element  //button[@data-qa-id='btn-login-fb']
    click element  //button[@data-qa-id='btn-login-fb']
    sleep  3s
    select window  NEW
    maximize browser window
    wait until page contains element  //input[@name='login']
    click element  //input[@name='login']
    sleep  5s
    close browser

TC_Invalid_FB_Login_2
    open browser  ${URL}  ${Browser}
    wait until page contains element  //button[@data-qa-id='btn-login-fb']
    click element  //button[@data-qa-id='btn-login-fb']
    sleep  3s
    select window  NEW
    maximize browser window
    wait until page contains element  //input[@name='email']
    input text  id:email  claudiaartia@yahoo.co.id
    wait until page contains element  //input[@name='login']
    click element  //input[@name='login']
    sleep  5s
    close browser

TC_Invalid_FB_Login_3
    open browser  ${URL}  ${Browser}
    wait until page contains element  //button[@data-qa-id='btn-login-fb']
    click element  //button[@data-qa-id='btn-login-fb']
    sleep  3s
    select window  NEW
    maximize browser window
    wait until page contains element  //input[@name='pass']
    input text  id:pass  siclaudia
    wait until page contains element  //input[@name='login']
    click element  //input[@name='login']
    sleep  5s
    close browser

TC_Valid_Login_Google
    open browser  ${URL}  ${Browser}
    wait until page contains element  //button[@data-qa-id='btn-login-google']
    click element  //button[@data-qa-id='btn-login-google']
    sleep  3s
    select window  NEW
    maximize browser window
    sleep  2s
    wait until page contains element  //input[@id='identifierId']
    input text  id:identifierId  artiasiregar@gmail.com
    click element  //content[@class='CwaK9']
    wait until page contains element  //input[@name='password']
    sleep  2s
    input password  name:password  bebekgendut
    #wait until page contains element  //content[@class='CwaK9']
    click element  //content[@class='CwaK9']
    sleep  5s
    close browser

TC_Invalid_Login_Google_1
    open browser  ${URL}  ${Browser}
    wait until page contains element  //button[@data-qa-id='btn-login-google']
    click element  //button[@data-qa-id='btn-login-google']
    sleep  3s
    select window  NEW
    maximize browser window
    wait until page contains element  //input[@name='identifier']
    click element  //content[@class='CwaK9']
    sleep  5s
    close browser

TC_Invalid_Login_Google_2
    open browser  ${URL}  ${Browser}
    wait until page contains element  //button[@data-qa-id='btn-login-google']
    click element  //button[@data-qa-id='btn-login-google']
    sleep  3s
    select window  NEW
    maximize browser window
    wait until page contains element  //input[@name='identifier']
    input text  name:identifier  artiasiregar@gmail.com
    click element  //content[@class='CwaK9']
    wait until page contains element  //input[@name='password']
    sleep  2s
    #wait until page contains element  //content[@class='CwaK9']
    click element  //content[@class='CwaK9']
    sleep  5s
    close browser

